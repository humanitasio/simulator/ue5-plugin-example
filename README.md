# UE5 Plugin Example

A Unreal Engine 5 plugin example based on the Third Person project template.

This project is a plugin wrap of a project template available by default when creating project in UE5 Editor.

## Getting Started

These instructions will get you a copy of the plugin up and running in your Unreal Engine 5 project.

To get the plugin to work, you only need to add it in your project plugin folder (e.g `MyProject/Plugins/Ue5PluginExample/Ue5PluginExample.uplugin`).

1. If your project is hosted using Git, add the plugin as a submodule

    ```bash
    cd MyProject/Plugins
    git submodule add https://gitlab.com/humanitasio/simulator/ue5-plugin-example.git Ue5PluginExample
    ```

2. If your project is not hosted using Git, clone the plugin as a normal repository

    ```bash
    cd MyProject/Plugins
    git clone https://gitlab.com/humanitasio/simulator/ue5-plugin-example.git Ue5PluginExample
    ```

Note that, if the plugin include source files, you will have to compile the project before opening it.

## License

This project is licensed under Unreal Engine EULA - see the [LICENSE](LICENSE) file for details.

## Related

- [Unreal Engine](https://www.unrealengine.com)
- [Third Person Template](https://docs.unrealengine.com/5.0/en-US/third-person-template-in-unreal-engine/)
