// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Ue5ThirdPersonGameMode.generated.h"

UCLASS(minimalapi)
class AUe5ThirdPersonGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUe5ThirdPersonGameMode();
};



