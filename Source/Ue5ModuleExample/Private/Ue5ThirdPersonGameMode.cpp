// Copyright Epic Games, Inc. All Rights Reserved.

#include "Ue5ThirdPersonGameMode.h"
#include "Ue5ThirdPersonCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUe5ThirdPersonGameMode::AUe5ThirdPersonGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Ue5PluginExample/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
